#!/bin/sh 

set -e

# called by uscan with '--upstream-version' <version> <file>
echo "version $2"
package=`dpkg-parsechangelog | sed -n 's/^Source: //p'`
version=$2
tarball=$3
TAR=${package}_${version}.orig.tar.gz
DIR=${package}-${version}.orig
upstream_package="$(echo $package | sed -e 's/^lib//' -e 's/-java$/-lru/')"
upstream_version="$(echo $version | sed 's/~/_/')"
REPO="http://concurrentlinkedhashmap.googlecode.com/svn/tags/${upstream_package}-${upstream_version}/"

svn export $REPO $DIR
GZIP=--best tar --exclude=lib --numeric --group 0 --owner 0 -cvzf $TAR $DIR

rm -rf $tarball $DIR
